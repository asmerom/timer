/**
 * Created by Asmerom on 12/14/15.
 */



$(document).ready(function(){

    UnoPico.ready(function($statev1){


        messages = [];

        checkState();

        function checkState() {
            $.each($statev1.get("messages", []), function(index, item){
                messages.push(item);
                $(".saved_container ul").append("<li>" + item.name + " - " + item.time + "</li>")
            })
        }

        $('form#add').on('submit', function(e){

            var message = $("#message").val();
            var time    = parseInt($("#time").val());

            e.preventDefault();

            if (message != "" && time != ""){

                var obj = {"name":message, "time":time};
                messages.push(obj);

                $("#message").val("");
                $("#time").val("");

                // append to the UI
                $(".saved_container ul").append("<li>" + message + " - " + time + "</li>");
            }

        });



        $('form#save').on('submit', function(e){
            e.preventDefault();


            $statev1.set({"messages": messages});


        });


    })


});